# container-and-dependency

To show/train on Container and Dependency Scanning. Including auto-remediation. See README for more information.

In a the pipeline security tab, look for a `High` severity `Container Scanning` item titled `CVE-2019-3462 in apt` to show resolving through merge request.
